function TrDec
%%% TrDec.m
%%% Counterexample to trend decomposition via weather types
%%
%% 2 types, fj frequency, Cj intensity
%% f1 + f2 = 1

f1 = @sin ;
f2 = @(x) 1 - f1(x)  ;
C1 = @(x) 1/pi * x ;
C2 = @(x) 1 - C1(x)  ; % for simplicity, C1 + C2 = 1

n = 100 ;
x = linspace(0, pi, n) ;

%% trends
[P.f1 S.f1] = polyfit(x, f1(x), 1) ;
[P.f2 S.f2] = polyfit(x, f2(x), 1) ;
[P.C1 S.C1] = polyfit(x, C1(x), 1) ;
[P.C2 S.C2] = polyfit(x, C2(x), 1) ;

%% plots
figure(1) ; clf ;

%% 2 types
subplot(2,1,1) ; hold on
plot(x, C1(x), 'k', 'linewidth', 3) ;
plot(x, C2(x), 'k--', 'linewidth', 3) ;
plot(x, f1(x), 'k', 'linewidth', 1) ;
plot(x, f2(x), 'k--', 'linewidth', 1) ;
plot(x, f1(x).*C1(x) + f2(x).*C2(x), 'color', 0.7*[1 1 1], 'linewidth', 3) ;
legend({'$$C_1$$' '$$C_2$$' '$$f_1$$' '$$f_2$$' '$$\bar {C}$$'}, 'interpreter', 'latex', 'location', 'northeastoutside') ; legend boxoff ;
xlabel('time') ; ylabel('C units') ;
h = title('2 types and corresponding {\it f} and {\it C}') ;

subplot(2,1,2) ; hold on
plot(x, polyval(P.C1, x), 'k', 'linewidth', 3) ;
plot(x, polyval(P.C2, x), 'k--', 'linewidth', 3) ;
plot(x, polyval(P.f1, x), 'k', 'linewidth', 1) ;
plot(x, polyval(P.f2, x), 'k--', 'linewidth', 1) ;
legend({'$$C_1$$' '$$C_2$$' '$$f_1$$' '$$f_2$$'}, 'interpreter', 'latex', 'location', 'northeastoutside') ; legend boxoff
xlabel('time') ; ylabel('C units') ;
title('true single {\it f} and {\it C} trends') ;
print -dpng Counterexample.png ;
print -dsvg Counterexample.svg ;
print -depsc Counterexample.eps ;

figure(2) ; clf ;
%% Beck et al. 2007, 3.3, 4.3.1
subplot(2,1,1) ; hold on ;
[f, C] = Bea_2007(f1(x), C1(x), f2(x), C2(x)) ;
P1.f = polyfit(x, f, 1) ;
P1.C = polyfit(x, C, 1) ;
plot(x, C, 'k', x, polyval(P1.C, x), 'k', 'linewidth', 3) ;
plot(x, f, 'k', x, polyval(P1.f, x), 'k', 'linewidth', 1) ;
ylim([-1.5 0.6]) ;
legend({'C dec.' 'C dec. trend' 'f dec.' 'f dec. trend'}, 'location', 'northeastoutside') ; legend boxoff
xlabel('time') ; ylabel('C units') ;
title('Beck et al. 2007 decomposition') ;

%% Widmann & Schär 1997, 7.1
subplot(2,1,2) ; hold on
[f, C, I] = WS_1997(f1(x), C1(x), f2(x), C2(x)) ;
P2.f = polyfit(x, f, 1) ;
P2.C = polyfit(x, C, 1) ;
P2.I = polyfit(x, I, 1) ;
plot(x, C, 'k', 'linewidth', 3) ;
plot(x, polyval(P2.C, x), 'k', 'linewidth', 3) ;
plot(x, f, 'k', 'linewidth', 1) ;
plot(x, polyval(P2.f, x), 'k', 'linewidth', 1) ;
plot(x, I, 'k--', 'linewidth', 1) ;
plot(x, polyval(P2.I, x), 'k--', 'linewidth', 1) ;
ylim([-1.5 0.6]) ;
legend({'C dec.' 'C dec. trend' 'f dec.' 'f dec. trend' 'I' 'I trend'}, 'location', 'northeastoutside') ; legend boxoff
xlabel('time') ; ylabel('C units') ;
title('Widmann and Schär 1997 decomposition') ;

print -dpng TrDec.png ;
print -dsvg TrDec.svg ;
print -depsc TrDec.eps ;

end

%% usage: [f, C] = Bea_2007 (f1, C1, f2, C2)
%%
%%
function [f, C] = Bea_2007 (f1, C1, f2, C2)
   df1 = [0 diff(f1)] ;
   df2 = [0 diff(f2)] ;
   dC1 = [0 diff(C1)] ;
   dC2 = [0 diff(C2)] ;

   C = cumsum(f1 .* dC1 + f2 .* dC2) ; % C decomposed
   f = cumsum(df1 .* (C1 + dC1) + df2 .* (C2 + dC2)) ; % frequency decomposed
end

%% usage: [f, C, I] = WS_1997 (f1, C1, f2, C2)
%%
%%
function [f, C, I] = WS_1997 (f1, C1, f2, C2)
   f10 = mean(f1(:)) ;
   f20 = mean(f2(:)) ;
   C10 = mean(C1(:)) ;
   C20 = mean(C2(:)) ;
   f1a = f1 - f10 ;
   f2a = f2 - f20 ;
   C1a = C1 - C10 ;
   C2a = C2 - C20 ;
   f = C10*f1a + C20*f2a ;
   C = f10*C1a + f20*C2a ;
   I = f1a.*C1a + f2a.*C2a ;
end
